# -*- coding: utf-8 -*-

from argparse import ArgumentParser, RawTextHelpFormatter
from os import listdir, mkdir, remove
from os.path import join, isfile, basename
from re import compile, IGNORECASE

from PIL import Image, ImageFilter


def list_dir(path):
    dirs = []
    files = []
    for item in listdir(path):
        full = join(path, item)
        if isfile(full):
            files.append(full)
        else:
            dirs.append(full)
    return files, dirs


def make_dir(new_dir):
    try:
        mkdir(new_dir)
    except Exception:
        pass


def img_resize(img, new_size, func=min, np=False):
    if np:
        return img.resize(new_size, Image.ANTIALIAS)
    else:
        w, h = img.size
        prop = func([new_size[0] / w, new_size[1] / h])
        new_size = int(round(prop * w, 0)), int(round(prop * h, 0))
        return img.resize(new_size, Image.ANTIALIAS)


def gen_bg(image, size, color):
    if color.lower().startswith('blur'):
        resized = img_resize(image, size, max)
        blur = resized.filter(ImageFilter.GaussianBlur(int((color.split(' ') + [0])[1]) or 20))
        w, h = blur.size
        new_w, new_h = size
        left = (w - new_w) / 2
        top = (h - new_h) / 2
        right = (w + new_w) / 2
        bottom = (h + new_h) / 2
        crop = blur.crop((left, top, right, bottom))
        return crop
    else:
        return Image.new('RGB', (size[0], size[1]), color)


class Resize():
    def __init__(self, path, remove, background, sizes, no_proportions):
        self.path = path
        self.rm = remove
        self.bg = background
        self.sizes = sizes
        self.np = no_proportions

    def start(self):
        if self.bg and self.np: print('\n\n\n\tAtenção! opção "background" desabilitada pela "no_proportions" .\n\n\n')
        root = list_dir(self.path)
        reg = compile(r'.+\.(jpg|jpeg|png|tif|bmp)$', IGNORECASE)
        for dir in root[1]:
            files, dirs = list_dir(dir)
            for file in files:
                if reg.match(file):
                    try:
                        print(file)
                        error = False
                        image = Image.open(file)
                        name = basename(file)
                        for new_size in self.sizes:
                            new_size = new_size.lower()
                            new_w, new_h = new_size.split('x')
                            new_w = int(new_w)
                            new_h = int(new_h)
                            new_dir = join(dir, new_size)
                            make_dir(new_dir)
                            save_path = join(new_dir, name)
                            try:
                                resized_image = img_resize(image, (new_w, new_h), min, self.np)
                                if self.bg and not self.np:
                                    image_bg = gen_bg(image, (new_w, new_h), self.bg)
                                    left = int((new_w - resized_image.size[0]) / 2)
                                    upper = int((new_h - resized_image.size[1]) / 2)
                                    image_bg.paste(resized_image, (left, upper))
                                    resized_image = image_bg
                                resized_image.save(save_path)
                            except Exception as e:
                                error = True
                                print('ERRO!\t',e, save_path)
                        image.close()
                        if self.rm and not error:
                            remove(file)
                    except Exception as e:
                        print('ERRO!\t', e, file)


if __name__ == '__main__':
    parser = ArgumentParser(description="Jeff's Image Resize", epilog='Exemplo: python -m resize -p /path/to/images -s 1024x1024 860x640 640x640 240x480 -b "blur 20"', formatter_class=RawTextHelpFormatter)
    parser.add_argument('-p', '--path', metavar='', required=True, type=str, help='Caminho para pasta raiz')
    parser.add_argument('-r', '--remove', action='store_true', help='Remover imagens completamente processsadas')
    parser.add_argument('-b', '--background', metavar='', default=None, type=str, help='Cor de fundo ou "blur [intensidade]", exempo: "#ffffff" ou "blur 20"')
    parser.add_argument('-s', '--sizes', metavar='', required=True, nargs='+', type=str, help='Lista com tamanhos para gerar')
    parser.add_argument('-np', '--no_proportions', action='store_true', help='NÃO mantem as proporções das imagens')
    args = parser.parse_args()

    try:
        r = Resize(args.path, args.remove, args.background, args.sizes, args.no_proportions)
        r.start()
    except KeyboardInterrupt:
        print('Ctrl+C')
        exit(1)
